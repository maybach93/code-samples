//
//  Модуль отображения новостей
//  Created by Vitalii Poponov on 17/10/2017.
//

protocol AdvertisementsBusinessLogic {
	func fetchAdvertisements(request: Advertisements.FetchAdvertisements.Request)
}

// Класс для описания бизнес-логики модуля Advertisements

class AdvertisementsInteractor: AdvertisementsBusinessLogic {
	let presenter: AdvertisementsPresentationLogic
	let provider: AdvertisementsProviderProtocol

	init(presenter: AdvertisementsPresentationLogic, provider: AdvertisementsProviderProtocol = AdvertisementsProvider()) {
		self.presenter = presenter
		self.provider = provider
	}
	
	// MARK: Do fetchAdvertisements
    
	func fetchAdvertisements(request: Advertisements.FetchAdvertisements.Request) {
		provider.getAdvertisements { (items, error) in
			let result: Advertisements.AdvertisementsRequestResult
			if let items = items {
				result = .success(items)
			} else if let error = error {
				result = .failure(.someError(message: error.localizedDescription))
			} else {
				result = .failure(.someError(message: "No Data"))
			}
			self.presenter.presentAdvertisements(response: Advertisements.FetchAdvertisements.Response(result: result))
		}
	}
}
