//
//  Модуль отображения новостей
//  Created by Vitalii Poponov on 17/10/2017.
//

import UIKit

protocol AdvertisementsDisplayLogic: class {
	func displayAdvertisements(viewModel: Advertisements.FetchAdvertisements.ViewModel)
}

class AdvertisementsViewController: ABAMBaseController {
	let interactor: AdvertisementsBusinessLogic
	var state: Advertisements.ViewControllerState

    var rootView: AdvertisementsView! {
        get {
            return view as! AdvertisementsView
        }
    }
    
	init(interactor: AdvertisementsBusinessLogic, initialState: Advertisements.ViewControllerState = .loading) {
		self.interactor = interactor
		self.state = initialState
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: Lifecycle
    
	override func loadView() {
		let view = AdvertisementsView(frame: UIScreen.main.bounds)
		self.view = view
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		doFetchAdvertisements()
	}

	// MARK: fetchAdvertisements
    
	func doFetchAdvertisements() {
		let request = Advertisements.FetchAdvertisements.Request()
		interactor.fetchAdvertisements(request: request)
	}
}

extension AdvertisementsViewController: AdvertisementsDisplayLogic {
	func displayAdvertisements(viewModel: Advertisements.FetchAdvertisements.ViewModel) {
		display(newState: viewModel.state)
	}

	func display(newState: Advertisements.ViewControllerState) {
		state = newState
		switch state {
		case .loading:
			startActivity()
		case let .error(message):
			stopActivity()
			ABErrorProcessor.shared().presentError(withStringDesription: message, completion: nil)
		case let .result(viewModel):
			stopActivity()
			rootView.showAdvertismentView(viewModel: viewModel, delegate: self)
		case .emptyResult:
			stopActivity()
		}
	}
}

extension AdvertisementsViewController: AdvertisementListDelegate {
    func didSelectItem(at indexPath: IndexPath) {
        print("show full advertisement")
    }
}

