//
//  Модуль отображения новостей
//  Created by Vitalii Poponov on 17/10/2017.
//

class AdvertisementsBuilder: ModuleBuilder {
    
	var initialState: Advertisements.ViewControllerState?
	
	func set(initialState: Advertisements.ViewControllerState) -> AdvertisementsBuilder {
		self.initialState = initialState
		return self
	}
	
	func build() -> UIViewController {
		guard let initialState = initialState else {
			fatalError("Initial state parameter was not set")
		}
		
		let presenter = AdvertisementsPresenter()
		let interactor = AdvertisementsInteractor(presenter: presenter)
		let controller = AdvertisementsViewController(interactor: interactor, initialState: initialState)

		presenter.viewController = controller
		return controller
	}
}
