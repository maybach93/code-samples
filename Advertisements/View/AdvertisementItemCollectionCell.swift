//
//  Created by Vitalii Poponov on 17/10/2017.
//

import UIKit

protocol AdvertisementItemCollectionCellProtocol: class {
    var isHighlightedCell: Bool { get set }
}

extension AdvertisementItemCollectionCell {
    struct Appearance {
        let topOffset: CGFloat = 10
        let leftRightOffset: CGFloat = 12
        let nameTopOffset: CGFloat = 2
    }
}

class AdvertisementItemCollectionCell: UICollectionViewCell, AdvertisementItemCollectionCellProtocol {
    
    //MARK: - Variables
    
    let appearance = Appearance()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.layer.cornerRadius = 6.0
        return view
    }()
    
    lazy var highlightView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.2
        view.isHidden = true
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.textColor = UIColor.white
        view.font = UIFont.systemFontOfSize13()
        return view
    }()
    
    lazy var descriptionLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.textColor = UIColor.white()
        view.font = UIFont.systemFontOfSize22()
        return view
    }()
    
    var isHighlightedCell: Bool {
        set {
            highlightView.isHidden = !isHighlighted
        }
        get {
            return !highlightView.isHidden
        }
    }
    
    //MARK: - Initializer
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubviews()
        makeConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private methods
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(highlightView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(descriptionLabel)
        containerView.bringSubview(toFront: highlightView)
    }
    
    private func makeConstraints() {
        
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        highlightView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(self.snp.top).offset(appearance.nameTopOffset)
            make.left.equalTo(self.snp.left).offset(appearance.leftRightOffset)
            make.right.equalTo(self.snp.right).offset(appearance.leftRightOffset)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(self.nameLabel.snp.bottom).offset(appearance.topOffset)
            make.left.equalTo(self.snp.left).offset(appearance.leftRightOffset)
            make.right.equalTo(self.snp.right).offset(appearance.leftRightOffset)
        }
    }
    
    //MARK: - Public
    
    func configure(with viewModel: AdvertisementViewModelProtocol) {
        nameLabel.text = viewModel.name
        descriptionLabel.text = viewModel.description
    }
}
