//
//  Created by Vitalii Poponov on 17/10/2017.
//

import UIKit

protocol AdvertisementListDelegate: class {
    func didSelectItem(at indexPath: IndexPath)
}

extension AdvertisementCollectionView {
    struct Configuration {
        static let cellReuseIdentifier = "\(AdvertisementItemCollectionCell.self)"
        static let minimumInteritemSpacing: CGFloat = 10
        static let contentInset = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
    }
}

class AdvertisementCollectionView: UIView {

    //MARK: - Variables
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.abClear()
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = Configuration.contentInset
        collectionView.register(AdvertisementItemCollectionCell.self, forCellWithReuseIdentifier: Configuration.cellReuseIdentifier)
        return collectionView
    }()

    fileprivate var model: AdvertisementListViewModelProtocol?
    weak var delegate: AdvertisementListDelegate?
    
    //MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        makeConstraints()
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: AdvertisementListViewModelProtocol, delegate: AdvertisementListDelegate) {
        self.model = viewModel
        self.delegate = delegate
        collectionView.reloadData()
    }
    
    //MARK: - Private methods
    
    private func addSubviews() {
        addSubview(collectionView)
    }
    
    private func makeConstraints() {
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func configureView() {
        backgroundColor = UIColor.abHexPaleGrey()
    }
}

extension AdvertisementCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectItem(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell: AdvertisementItemCollectionCellProtocol = collectionView.cellForItem(at: indexPath) as? AdvertisementItemCollectionCellProtocol  {
            cell.isHighlightedCell = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell: AdvertisementItemCollectionCellProtocol = collectionView.cellForItem(at: indexPath) as? AdvertisementItemCollectionCellProtocol  {
            cell.isHighlightedCell = false
        }
    }
}

extension AdvertisementCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: Configuration.cellReuseIdentifier, for: indexPath)
        if let viewModel = model?.viewModel(at: indexPath) {
            (collectionCell as? AdvertisementItemCollectionCell)?.configure(with: viewModel)
        }
        return collectionCell
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.numberOfItemsInSection(section: section) ?? 0
    }
}

extension AdvertisementCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return model?.sizeForItem(at: indexPath) ?? CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Configuration.minimumInteritemSpacing
    }
}
