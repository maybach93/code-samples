//
//  Created by Vitalii Poponov on 17/10/2017.
//

import UIKit

extension AdvertisementsView {
	struct Appearance {
		let offset: CGFloat = 10
	}
}

class AdvertisementsView: UIView {
	let appearance = Appearance()
	
    var advertisementView = AdvertisementCollectionView()
    
    override init(frame: CGRect = CGRect.zero) {
		super.init(frame: frame)
		addSubviews()
		makeConstraints()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func addSubviews(){
		addSubview(advertisementView)
	}
	
	func makeConstraints() {
		advertisementView.snp.makeConstraints{ make in
			make.edges.equalTo(self).offset(appearance.offset)
		}
	}
    
    func showAdvertismentView(viewModel: AdvertisementListViewModelProtocol, delegate: AdvertisementListDelegate) {
        advertisementView.configure(viewModel: viewModel, delegate: delegate)
    }
}
