//
//  Created by Vitalii Poponov on 17/10/2017.
//

import Foundation

struct AdvertisementViewModel: AdvertisementViewModelProtocol {

    let name: String
    let description: String
    
    init(model: AdvertisementsModel) {
        name = model.name
        description = model.description
    }
}

protocol AdvertisementViewModelProtocol {
    var name: String { get }
    var description: String { get }
}

protocol AdvertisementListViewModelProtocol {
    var advertisements: [AdvertisementViewModel] { get }
    func numberOfItemsInSection(section: Int) -> Int
    func sizeForItem(at indexPath: IndexPath) -> CGSize
    func viewModel(at indexPath: IndexPath) -> AdvertisementViewModelProtocol?
}

extension AdvertisementListViewModel {
    struct Configuration {
        static let cellSize: CGSize = CGSize.init(width: 180, height: 90)
    }
}

struct AdvertisementListViewModel: AdvertisementListViewModelProtocol {
   
    var advertisements: [AdvertisementViewModel]
    
    //MARK: - Init
    
    init(advertisements: [AdvertisementsModel]) {
        self.advertisements = Array(advertisements.enumerated().map {
            return AdvertisementViewModel(model: $1)
        })
    }
    
    //MARK: - Public 
    
    func numberOfItemsInSection(section: Int) -> Int {
        return advertisements.count
    }
    
    func viewModel(at indexPath: IndexPath) -> AdvertisementViewModelProtocol? {
        return advertisements[indexPath.row]
    }
    
    func sizeForItem(at indexPath: IndexPath) -> CGSize {
        return Configuration.cellSize
    }
}
