//
//  Created by Vitalii Poponov on 17/10/2017.
//

// Модель данных, описывающая advertisement

struct AdvertisementsModel {
	
	let name: String
    let description: String
}
