//
//  Created by Vitalii Poponov on 17/10/2017.
//

protocol AdvertisementsServiceProtocol {
	func fetchAdvertisements(completion: @escaping ([AdvertisementsModel]?, Error?) -> Void)
}

// Получает данные для модуля Advertisements

class AdvertisementsService: AdvertisementsServiceProtocol {
	let translator: AdvertisementsTranslator
	let apiClient: APIClient
	
	init(apiClient: APIClient = APIClientRegister.shared.defaultClient(),
	     translator: AdvertisementsTranslator = AdvertisementsTranslator()) {
		self.apiClient = apiClient
		self.translator = translator
	}

	func fetchAdvertisements(completion: @escaping ([AdvertisementsModel]?, Error?) -> Void) {
		apiClient.get(endpoint: "advertisements/list", withParameters: [:]) { (result: ParsedResult<[[String: Any]]>) in
			switch result{
			case let .success(json):
				do {
					let models = try self.translator.translateFrom(array: json)
					completion(models, nil)
				} catch (let error) {
					completion(nil, error)
				}
			case let .failure(error):
				completion(nil, error)
			}
		}
	}
}
