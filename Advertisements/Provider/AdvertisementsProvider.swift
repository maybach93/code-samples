//
//  Created by Vitalii Poponov on 17/10/2017.
//

protocol AdvertisementsProviderProtocol {
	func getAdvertisements(completion: @escaping ([AdvertisementsModel]?, AdvertisementsProviderError?) -> Void)
}

enum AdvertisementsProviderError: Error {
	case getItemsFailed(underlyingError: Error)
}

// Отвечает за получение данных модуля Advertisements

struct AdvertisementsProvider: AdvertisementsProviderProtocol {
	let dataStore: AdvertisementsDataStore
	let service: AdvertisementsServiceProtocol

	init(dataStore: AdvertisementsDataStore = AdvertisementsDataStore(), service: AdvertisementsServiceProtocol = AdvertisementsService()) {
		self.dataStore = dataStore
		self.service = service
	}

	func getAdvertisements(completion: @escaping ([AdvertisementsModel]?, AdvertisementsProviderError?) -> Void) {
		if dataStore.models?.isEmpty == false {
			return completion(self.dataStore.models, nil)
		}
		service.fetchAdvertisements { (array, error) in
			if let error = error {
				completion(nil, .getItemsFailed(underlyingError: error))
			} else if let models = array {
				self.dataStore.models = models
				completion(self.dataStore.models, nil)
			}
		}
	}
}
