//
//  Created by Vitalii Poponov on 17/10/2017.
//

// Декодирует ответ от сервера в модель приложения и обратно

class AdvertisementsTranslator: Translator {
    
	// перечисление ключей json объекта в ответе сервера
    
	enum AdvertisementsKeys: String {
		case name
		case description
	}

	func translateFrom(dictionary json: [String : Any]) throws -> AdvertisementsModel {
		return try AdvertisementsModel(
			name: json.getValueWithCast(key: AdvertisementsKeys.name.rawValue),
			description: json.getValueWithCast(key: AdvertisementsKeys.description.rawValue)
		)
	}

	func translateToDictionary(_ object: AdvertisementsModel) -> [String : Any] {
		return [AdvertisementsKeys.name.rawValue: object.name,
				AdvertisementsKeys.description.rawValue: object.description]
	}
}
