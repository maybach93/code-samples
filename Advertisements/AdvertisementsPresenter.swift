//
//  Модуль отображения новостей
//  Created by Vitalii Poponov on 17/10/2017.
//

import UIKit

protocol AdvertisementsPresentationLogic {
	func presentAdvertisements(response: Advertisements.FetchAdvertisements.Response)
}

// Отвечает за отображение данных модуля Advertisements

class AdvertisementsPresenter: AdvertisementsPresentationLogic {
	weak var viewController: AdvertisementsDisplayLogic?

	// MARK: Do fetchAdvertisements
    
	func presentAdvertisements(response: Advertisements.FetchAdvertisements.Response) {
		var viewModel: Advertisements.FetchAdvertisements.ViewModel
		
		switch response.result {
		case let .failure(error):
			viewModel = Advertisements.FetchAdvertisements.ViewModel(state: .error(message: error.localizedDescription))
		case let .success(result):
			if result.isEmpty {
				viewModel = Advertisements.FetchAdvertisements.ViewModel(state: .emptyResult)
			} else {
                let advertismentViewModel = AdvertisementListViewModel(advertisements: result)
				viewModel = Advertisements.FetchAdvertisements.ViewModel(state: .result(advertismentViewModel))
			}
		}
		
		viewController?.displayAdvertisements(viewModel: viewModel)
	}
}
