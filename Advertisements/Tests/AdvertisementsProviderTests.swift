//
//  SUT: AdvertisementsProvider
//
//  Collaborators:
//  AdvertisementsService
//  AdvertisementsDataStore
//

import Quick
import Nimble

class AdvertisementsProviderTests: QuickSpec {
	override func spec() {
		var provider: AdvertisementsProvider!
		var serviceMock: AdvertisementsServiceMock!
		var dataStoreMock: AdvertisementsDataStoreMock!
		
		var getItemsResult: (items: [AdvertisementsModel]?, error: AdvertisementsProviderError?)

		beforeEach {
			serviceMock = AdvertisementsServiceMock()
			dataStoreMock = AdvertisementsDataStoreMock()
			provider = AdvertisementsProvider(dataStore: dataStoreMock, service: serviceMock)
			
			getItemsResult = (nil, nil)
		}

		describe(".getItems") {
			context("cache is empty") {
				beforeEach {
					dataStoreMock.models = nil
				}
				
				it ("should request data from service") {
					// when
					provider.getAdvertisements { (_, _) in }
					// then
					expect(serviceMock.fetchItemsDidCalled).to(equal(1))
				}
				
				context("successfull response"){
					it("should save data to store"){
						// given
						serviceMock.fetchItemsCompletionStub = (TestData.responseData, nil)
						// when
						provider.getAdvertisements { (_, _) in }
						// then
						expect(dataStoreMock.models).to(equal(TestData.responseData))
					}
					
					it("should return result in callback"){
						// given
						serviceMock.fetchItemsCompletionStub = (TestData.responseData, nil)
						// when
						provider.getAdvertisements { getItemsResult = ($0, $1) }
						// then
						expect(getItemsResult.items).to(equal(TestData.responseData))
						expect(getItemsResult.error).to(beNil())
					}
				}
				
				context("failed response"){
					it("should not update store"){
						// given
						serviceMock.fetchItemsCompletionStub = (nil, TestData.responseError)
						// when
						provider.getAdvertisements { (_, _) in }
						// then
						expect(dataStoreMock.models).to(beNil())
					}
					
					it("should return error in callback"){
						// given
						serviceMock.fetchItemsCompletionStub = (nil, TestData.responseError)
						// when
						provider.getAdvertisements { getItemsResult = ($0, $1) }
						// then
						expect(getItemsResult.items).to(beNil())
						expect{ if case .getItemsFailed(_)? = getItemsResult.error { return true }; return false }.to(beTrue())
					}
				}
			}
		}
		
		context("cache fulfilled"){
			it("should not call service"){
				// given
				dataStoreMock.models = TestData.responseData
				// when
				provider.getAdvertisements { (_, _) in }
				// then
				expect(serviceMock.fetchItemsDidCalled).to(equal(0))
			}
			
			it("should return data in callback"){
				// given
				dataStoreMock.models = TestData.responseData
				// when
				provider.getAdvertisements { getItemsResult = ($0, $1) }
				// then
				expect(getItemsResult.items).to(equal(TestData.responseData))
				expect(getItemsResult.error).to(beNil())
			}
		}
	}
}

extension AdvertisementsProviderTests {
	enum TestData {
		static let responseData = AdvertisementsModelTests.TestData.entitiesCollection()
		static let responseError = APIClientError.other
	}
}

fileprivate class AdvertisementsServiceMock: AdvertisementsServiceProtocol {
	var fetchItemsDidCalled: Int = 0
	var fetchItemsArguments: (([AdvertisementsModel]?, APIClientError?) -> Void)?
	var fetchItemsCompletionStub: (result: [AdvertisementsModel]?, error: APIClientError?)
	
	func fetchAdvertisements(completion: @escaping ([AdvertisementsModel]?, Error?) -> Void) {
		fetchItemsDidCalled += 1
		fetchItemsArguments = completion
		completion(fetchItemsCompletionStub.result, fetchItemsCompletionStub.error)
	}
}

fileprivate class AdvertisementsDataStoreMock: AdvertisementsDataStore {
	
}

fileprivate class ErrorMock: Error {}
