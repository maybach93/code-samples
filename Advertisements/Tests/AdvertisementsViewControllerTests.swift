//
//  SUT: AdvertisementsViewController
//
//  Collaborators:
//  AdvertisementsInteractor
//

import Quick
import Nimble

class AdvertisementsViewControllerTests: QuickSpec {
	override func spec() {
		var viewController: AdvertisementsViewController!
		var interactorMock: AdvertisementsInteractorMock!

		beforeEach {
			interactorMock = AdvertisementsInteractorMock()
			viewController = AdvertisementsViewController(interactor: interactorMock)
		}

		describe(".doSomething") {
			it("should call method in interactor") {
				// when
				viewController.doFetchAdvertisements()

				// then
				expect(interactorMock.doFetchAdvertisementsDidCalled).to(equal(1))
				expect(interactorMock.doFetchAdvertisementsArguments).toNot(beNil())
			}
		}
	}
}

extension AdvertisementsViewControllerTests {
	enum TestData {
		static let request = Advertisements.FetchAdvertisements.Request()
	}
}

fileprivate class AdvertisementsInteractorMock: AdvertisementsBusinessLogic {
	var doFetchAdvertisementsDidCalled: Int = 0
	var doFetchAdvertisementsArguments: Advertisements.FetchAdvertisements.Request?

	func fetchAdvertisements(request: Advertisements.FetchAdvertisements.Request) {
		doFetchAdvertisementsDidCalled += 1
		doFetchAdvertisementsArguments = request
	}
}
