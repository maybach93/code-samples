//
//  SUT: AdvertisementsModel
//

import Quick
import Nimble

class AdvertisementsModelTests: QuickSpec {
	override func spec() {
		describe("equalit operator") {
			it("should return true for same objects"){
				expect(TestData.model.name == TestData.model.name).to(beTrue())
                expect(TestData.model.description == TestData.model.description).to(beTrue())
			}
		}
	}
}

extension AdvertisementsModelTests {
	enum TestData {
		static let name = "Some name"
		static let description = "Some description"
		static let model = AdvertisementsModel(name: name, description: description)
        
        static func entitiesCollection() -> [AdvertisementsModel] {
            return [model]
        }
	}
}
