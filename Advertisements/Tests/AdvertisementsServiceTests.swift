//
//  SUT: AdvertisementsService
//
//  Collaborators:
//  APIClient
//  AdvertisementsTranslator
//
import Quick
import Nimble

class AdvertisementsServiceTests: QuickSpec {
	override func spec() {
		var service: AdvertisementsService!
		var apiClientMock: APIClientMock!
		var translatorMock: AdvertisementsTranslatorMock!
		
		var fetchResult: (items: [AdvertisementsModel]?, error: Error?)!

		beforeEach{
			apiClientMock = APIClientMock()
			translatorMock = AdvertisementsTranslatorMock()
			service = AdvertisementsService(apiClient: apiClientMock, translator: translatorMock)
			
			fetchResult = (nil, nil)
		}

		describe(".fetchItems"){
			it("should get data with api client"){
				// when
				service.fetchAdvertisements{ (_, _) in }

				// then
				expect(apiClientMock.executeDidCalled).toEventually(equal(1))
			}
			
			context("success result") {
				it("should try to translate result into model"){
					// given
					apiClientMock.executeCallbackStub = TestData.successResult
					// when
					service.fetchAdvertisements { (_, _) in }
					// then
					expect(translatorMock.translateFromDictionaryDidCalled).to(equal(1))
				}
				
				context("translation successfull") {
					it("should return model"){
						// given
						apiClientMock.executeCallbackStub = TestData.successResult
						translatorMock.translateFromDictionaryStub = TestData.successfullTranslationResult
						// when
						service.fetchAdvertisements { fetchResult = ($0, $1) }
						// then
						expect(fetchResult.items).toNot(beNil())
						expect(fetchResult.error).to(beNil())
					}
				}
				
				context("translation failed") {
					it("should return error"){
						// given
						apiClientMock.executeCallbackStub = TestData.successResult
						// when
						service.fetchAdvertisements { fetchResult = ($0, $1) }
						// then
						expect(fetchResult.items).to(beNil())
						expect(fetchResult.error).toNot(beNil())
					}
				}
			}
			
			context("failure result") {
				it("should return error"){
					// given
					apiClientMock.executeCallbackStub = TestData.failureResult
					// when
					service.fetchAdvertisements { fetchResult = ($0, $1) }
					// then
					expect(fetchResult.items).to(beNil())
					expect(fetchResult.error).toNot(beNil())
				}
			}
		}
	}
}

extension AdvertisementsServiceTests {
	enum TestData {
		static let keys: [[String: Any]] = [[:]]
		static let error: APIClientError = .other
		static let successResult: ParsedResult<Any> = .success(keys)
		static let failureResult: ParsedResult<Any> = .failure(error)
		static let successfullTranslationResult: AdvertisementsModel = AdvertisementsModelTests.TestData.entitiesCollection().first!
	}
}

fileprivate class AdvertisementsTranslatorMock: AdvertisementsTranslator {
	var translateFromDictionaryDidCalled: Int = 0
	var translateFromDictionaryArguments: [String: Any]?
	var translateFromDictionaryStub: AdvertisementsModel?
	
	override func translateFrom(dictionary json: [String : Any]) throws -> AdvertisementsModel {
		translateFromDictionaryDidCalled = true
		translateFromDictionaryArguments = json
		guard let translateFromDictionaryStub = translateFromDictionaryStub else {
			throw TranslatorError.invalidJSONObject
		}
		return translateFromDictionaryStub
	}
}
