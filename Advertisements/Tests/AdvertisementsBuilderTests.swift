//
//  SUT: AdvertisementsBuilder
//
//  Collaborators:
//  AdvertisementsViewController
//  AdvertisementsInteractor
//  AdvertisementsPresenter
//  AdvertisementsProvider
//

import Quick
import Nimble

class AdvertisementsBuilderTests: QuickSpec {
	override func spec() {
		var builder: AdvertisementsBuilder!

		beforeEach {
			builder = AdvertisementsBuilder()
		}

		describe(".build") {
			it("should build module parts") {
				// when
				let controller = builder.set(initialState: TestData.initialState).build() as? AdvertisementsViewController
				let interactor = controller?.interactor as? AdvertisementsInteractor
				let presenter = interactor?.presenter as? AdvertisementsPresenter

				// then
				expect(controller).toNot(beNil())
				expect(interactor).toNot(beNil())
				expect(presenter).toNot(beNil())
			}
			
			it("should set dependencies between module parts") {
				// when
				let controller = builder.set(initialState: TestData.initialState).build() as? AdvertisementsViewController
				let interactor = controller?.interactor as? AdvertisementsInteractor
				let presenter = interactor?.presenter as? AdvertisementsPresenter
				
				// then
				expect(presenter?.viewController).to(beIdenticalTo(controller))
			}
		}
	}
}

extension AdvertisementsBuilderTests {
	enum TestData {
		static let initialState = Advertisements.ViewControllerState.loading
	}
}
