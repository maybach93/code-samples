//
//  SUT: AdvertisementsInteractor
//
//  Collaborators:
//  AdvertisementsProvider
//  AdvertisementsPresenter
//

import Quick
import Nimble

class AdvertisementsInteractorTests: QuickSpec {
	override func spec() {
		var interactor: AdvertisementsInteractor!
		var presenterMock: AdvertisementsPresenterMock!
		var providerMock: AdvertisementsProviderMock!

		beforeEach {
			providerMock = AdvertisementsProviderMock()
			presenterMock = AdvertisementsPresenterMock()
			interactor = AdvertisementsInteractor(presenter: presenterMock, provider: providerMock)
		}

		describe(".doSomething") {
			it("should get data from provider") {
				// when
				interactor.fetchAdvertisements(request: TestData.request)
				// then
				expect(providerMock.getItemsDidCalled).to(equal(1))
			}
			
			context("getItems successfull"){
				it("should prepare success response and call presenter"){
					// given
					providerMock.getItemsCompletionStub = (result: TestData.models, error: nil)
					// when
					interactor.fetchAdvertisements(request: TestData.request)
					// then
					expect(presenterMock.presentAdvertisementsDidCalled).to(equal(1))
					expect(presenterMock.presentAdvertisementsArguments).toNot(beNil())
					expect{ if case .success(_)? = presenterMock.presentAdvertisementsArguments?.result { return true }; return false }.to(beTruthy())
				}
			}
			
			context("getItems failed"){
				it("should prepare failed response and call presenter"){
					// given
					providerMock.getItemsCompletionStub = (result: nil, error: TestData.getItemsFailedError)
					// when
					interactor.fetchAdvertisements(request: TestData.request)
					// then
					expect(presenterMock.presentAdvertisementsDidCalled).to(equal(1))
					expect(presenterMock.presentAdvertisementsArguments).toNot(beNil())
					expect{ if case .failure(_)? = presenterMock.presentAdvertisementsArguments?.result { return true }; return false }.to(beTruthy())
				}
			}
		}
	}
}

extension AdvertisementsInteractorTests {
	enum TestData {
		static let request = Advertisements.FetchAdvertisements.Request()
		static let models = AdvertisementsModelTests.TestData.entitiesCollection()
		
		fileprivate static let underlyingError = ErrorMock()
		fileprivate static let getItemsFailedError = AdvertisementsProviderError.getItemsFailed(underlyingError: underlyingError)
	}
}

fileprivate class AdvertisementsProviderMock: AdvertisementsProviderProtocol {
	var getItemsDidCalled: Int = 0
	var getItemsArguments: (([AdvertisementsModel]?, AdvertisementsProviderError?) -> Void)?
	var getItemsCompletionStub: (result: [AdvertisementsModel]?, error: AdvertisementsProviderError?) = (nil, nil)
	
	func getAdvertisements(completion: @escaping ([AdvertisementsModel]?, AdvertisementsProviderError?) -> Void) {
		getItemsDidCalled += 1
		getItemsArguments = completion
		completion(getItemsCompletionStub.result, getItemsCompletionStub.error)
	}
}

fileprivate class AdvertisementsPresenterMock: AdvertisementsPresentationLogic {
	var presentAdvertisementsDidCalled: Int = 0
	var presentAdvertisementsArguments: Advertisements.FetchAdvertisements.Response?

	func presentAdvertisements(response: Advertisements.FetchAdvertisements.Response) {
		presentAdvertisementsDidCalled += 1
		presentAdvertisementsArguments = response
	}
}

fileprivate class ErrorMock: Error { }
