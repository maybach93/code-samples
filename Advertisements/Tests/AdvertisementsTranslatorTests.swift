//
//  SUT: AdvertisementsTranslator
//

import Quick
import Nimble

class AdvertisementsTranslatorTests: QuickSpec {
	override func spec() {
		var translator: AdvertisementsTranslator!

		beforeEach{
			translator = AdvertisementsTranslator()
		}

		describe(".tranlateFrom dictionary"){

			it("should throw error for invalid DTO"){
				expect{ try translator.translateFrom(dictionary: TestData.emptyKeys) }.to(throwError(CastExceptions.safeCastException("")))
			}

			it("should return data model for valid DTO"){
				// when
				let result = try? translator.translateFrom(dictionary: TestData.validKeys)
				// then
				expect(result?.name).to(equal(TestData.model.name))
				expect(result?.description).to(equal(TestData.model.description))
			}
		}

		describe(".translateTo dictionary") {
			it("should return valid DTO object"){
				// when
				let result = translator.translateToDictionary(TestData.model)
				// then
				expect(result[AdvertisementsTranslator.AdvertisementsKeys.name.rawValue] as? String).to(equal(TestData.name))
				expect(result[AdvertisementsTranslator.AdvertisementsKeys.description.rawValue] as? String).to(equal(TestData.description))
			}
		}
	}
}

extension AdvertisementsTranslatorTests {
	enum TestData {
		static let name = "Some advertisement"
		static let description = "Some description"
		
		static let emptyKeys: [String: Any] = [:]
		static let validKeys: [String: Any] = [AdvertisementsTranslator.AdvertisementsKeys.name.rawValue: name,
											  AdvertisementsTranslator.AdvertisementsKeys.description.rawValue: description]
		static let model: AdvertisementsModel = AdvertisementsModel(name: name,
																  description: description)
	}
}
