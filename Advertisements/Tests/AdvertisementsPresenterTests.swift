//
//  SUT: AdvertisementsPresenter
//
//  Collaborators:
//  AdvertisementsViewController
//

import Quick
import Nimble

class AdvertisementsPresenterTests: QuickSpec {
	override func spec() {
		var presenter: AdvertisementsPresenter!
		var viewControllerMock: AdvertisementsViewControllerMock!

		beforeEach {
			presenter = AdvertisementsPresenter()
			viewControllerMock = AdvertisementsViewControllerMock()
			presenter.viewController = viewControllerMock
		}

		describe(".presentSomething") {
			context("successfull empty result") {
				it ("should prepare empty view model and display it in view") {
					// when
					presenter.displayAdvertisements(response: TestData.successEmptyResponse)
					// then
					expect(viewControllerMock.displayAdvertisementsDidCalled).to(beTruthy())
					expect{ if case .emptyResult? = viewControllerMock.displayAdvertisementsArguments?.state { return true }; return false }.to(beTruthy())
				}
			}
			
			context("successfull result") {
				it ("should prepare result view model and display it in view") {
					// when
					presenter.displayAdvertisements(response: TestData.successResponse)
					// then
					expect(viewControllerMock.displayAdvertisementsDidCalled).to(beTruthy())
					expect{ if case .result(_)? = viewControllerMock.displayAdvertisementsArguments?.state { return true }; return false }.to(beTruthy())
				}
			}
			
			context("failure result") {
				it ("should prepare error view model and display it in view") {
					// when
					presenter.displayAdvertisements(response: TestData.failureResponse)
					// then
					expect(viewControllerMock.displayAdvertisementsDidCalled).to(beTruthy())
					expect{ if case .error(_)? = viewControllerMock.displayAdvertisementsArguments?.state { return true }; return false }.to(beTruthy())
				}
			}
		}
	}
}

extension AdvertisementsPresenterTests {
	enum TestData {
		static let successEmptyResponse = Advertisements.FetchAdvertisements.Response(result: .success([]))
        static let successResponse = Advertisements.FetchAdvertisements.Response(result: .success([AdvertisementsModel(name: "Some name", description: "Some description")]))
		static let failureResponse = Advertisements.FetchAdvertisements.Response(result: .failure(.someError(message: "some error")))
	}
}

fileprivate class AdvertisementsViewControllerMock: AdvertisementsDisplayLogic {
	var displayAdvertisementsDidCalled: Int = 0
	var displayAdvertisementsArguments: Advertisements.FetchAdvertisements.ViewModel?

	func displayAdvertisements(viewModel: Advertisements.FetchAdvertisements.ViewModel) {
		displayAdvertisementsDidCalled += 1
		displayAdvertisementsArguments = viewModel
	}
}
