//
//  Модуль отображения новостей
//  Created by Vitalii Poponov on 17/10/2017.
//

enum Advertisements {
    
	// MARK: Use cases
    
	enum FetchAdvertisements {
		struct Request {
		}

		struct Response {
			var result: AdvertisementsRequestResult
		}

		struct ViewModel {
			var state: ViewControllerState
		}
	}

	enum AdvertisementsRequestResult {
		case failure(AdvertisementsError)
		case success([AdvertisementsModel])
	}

	enum ViewControllerState {
		case loading
		case result(AdvertisementListViewModelProtocol)
		case emptyResult
		case error(message: String)
	}

	enum AdvertisementsError: Error {
		case someError(message: String)
	}
}

